Below is a list of books, articles, and essays on the political, and economic theory of private law societies, as well as strategies for bringing it about.

## The Law
http://bastiat.org/en/the_law.html

## Machinery of Freedom
http://daviddfriedman.com/The_Machinery_of_Freedom_.pdf

## Law's Order
http://www.daviddfriedman.com/laws_order/index.shtml

## The Private Production of Defense
https://mises.org/library/private-production-defense

## The Myth of National Defense
https://mises.org/library/myth-national-defense-essays-theory-and-history-security-production

## Democracy: The God That Failed
https://archive.org/details/HoppeDemocracyTheGodThatFailed

## The Economics and Ethics of Private Property
https://mises.org/library/economics-and-ethics-private-property-0

## A Theory of Socialism and Capitalism
https://mises.org/library/theory-socialism-and-capitalism-0

##  For a New Liberty
https://mises.org/library/new-liberty-libertarian-manifesto

## Chaos Theory
https://mises.org/library/chaos-theory

## Defending the Undefendable
https://mises.org/library/defending-undefendable

## Individualism and Economic Order
https://mises.org/library/individualism-and-economic-order

## The Idea of a Private Law Society
https://mises.org/library/idea-private-law-society

## Economics In One Lesson
http://www.hacer.org/pdf/Hazlitt00.pdf

## Basic Economics
https://archive.org/download/SowellThomasBasicEconomics5thEdition2014BasicBooks9780465056842/Sowell%2C%20Thomas%20-%20Basic%20Economics%20-%205th%20Edition%20%282014%2C%20Basic%20Books%2C%209780465056842%29.epub

## Man, Economy and State, with Power and Market
https://mises.org/library/man-economy-and-state-power-and-market

## Economic Science and the Austrian Method
https://mises.org/library/economic-science-and-austrian-method

## The Theory of Money and Credit
https://mises.org/library/theory-money-and-credit

## Human Action
https://mises-media.s3.amazonaws.com/Human%20Action_3.pdf

# Strategy
On strategies for bringing about a private law society.

## What Must Be Done
https://mises.org/library/what-must-be-done-0